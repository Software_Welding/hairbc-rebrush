/*
 * Annameryca.h
 *
 *  Created on: 08 feb 2019
 *      Author: Software Welding
 */

#ifndef ANNAMERYCA_H_
#define ANNAMERYCA_H_

//#define TEST_DARIO
#define MODIFICA_CONTACICLI_ACQUA
//#define MODIFICA_FLIPFLOP_ACQUA
#define MODIFICA_ASPETTA_ACQUA_LIVELLO_MASSIMO


#ifdef TEST_DARIO
#warning "Test HW attivato. Questo NON E' il programma di produzione."
#endif
#if defined(MODIFICA_CONTACICLI_ACQUA) && defined(MODIFICA_FLIPFLOP_ACQUA)
#error "Selezionare UNA SOLA tra le due opzioni gestione acqua nel file Annameryca.h"
#endif
#if !defined(MODIFICA_CONTACICLI_ACQUA) && !defined(MODIFICA_FLIPFLOP_ACQUA)
#error "Selezionare ALMENO UNA tra le due opzioni gestione acqua nel file Annameryca.h"
#endif




typedef enum stato_macchina_t {
	Stato_ATTESA = 1,
	Stato_MOTORE,
	Stato_POMPA,
	Stato_POMPA_II,
	Stato_ASCIUGATURA,
	Stato_STOP,
	Stato_ERROR = 99,
	Stato_ALLARME
} stato_macchina_t;

typedef enum onoff_t {
	Off,
	On
} onoff_t;

typedef enum buzzTone_t {
	Buzz_Ok,
	Buzz_Err
} buzzTone_t;

#endif /* ANNAMERYCA_H_ */
