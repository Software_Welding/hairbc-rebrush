/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define DIP_1_Pin GPIO_PIN_1
#define DIP_1_GPIO_Port GPIOA
#define DIP_2_Pin GPIO_PIN_2
#define DIP_2_GPIO_Port GPIOA
#define LED_R_Pin GPIO_PIN_3
#define LED_R_GPIO_Port GPIOA
#define LED_G_Pin GPIO_PIN_4
#define LED_G_GPIO_Port GPIOA
#define BUTTON_START_Pin GPIO_PIN_5
#define BUTTON_START_GPIO_Port GPIOA
#define BUZZER_Pin GPIO_PIN_6
#define BUZZER_GPIO_Port GPIOA
#define INT_H2O_LEVEL_HI_Pin GPIO_PIN_2
#define INT_H2O_LEVEL_HI_GPIO_Port GPIOB
#define INT_H2O_LEVEL_LO_Pin GPIO_PIN_10
#define INT_H2O_LEVEL_LO_GPIO_Port GPIOB
#define SAFE_COVER_INT_Pin GPIO_PIN_11
#define SAFE_COVER_INT_GPIO_Port GPIOB
#define LED_OK_COVER_Pin GPIO_PIN_12
#define LED_OK_COVER_GPIO_Port GPIOB
#define LED_ALARM_COVER_Pin GPIO_PIN_13
#define LED_ALARM_COVER_GPIO_Port GPIOB
#define EN_WATER_PUMP_Pin GPIO_PIN_14
#define EN_WATER_PUMP_GPIO_Port GPIOB
#define EN_PHON_Pin GPIO_PIN_15
#define EN_PHON_GPIO_Port GPIOB
#define EN_MOTOR_Pin GPIO_PIN_9
#define EN_MOTOR_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
