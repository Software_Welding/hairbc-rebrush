/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "Annameryca.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim16;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
#define POMPATE		5

#define POMPA_ON	25//20//17//15//12
#define POMPA_OFF	170//180

#define ASCIUGA_TIME	4200	//originale 25 secondi, 3500

volatile uint16_t conta_ciclo=100, ms_ciclo = 0, ms_alive = 0;
volatile uint16_t ms_buzz = 0;

uint8_t dummy = 0;

uint16_t i_pompa = 0;
uint8_t Tempi_Sel = 0;
uint16_t TempiPompa[4][10] = {
		{25,170,25,170,25,170, 0,  0, 0,  0},
		{20,340,15,340,15,100, 0,  0, 0,  0},
		{25,170,25,170,25,170,25,170, 0,  0},
		{25,170,25,170,25,170,25,170,25,100}
//		{25,340,25,340,25,100},
//		{30,400,25,10,1,10}
};
#ifdef MODIFICA_ASPETTA_ACQUA_LIVELLO_MASSIMO
typedef struct
{
	uint8_t enable;
	uint16_t delay;
	uint8_t numero_segnalazioni;
} Segnalazione_Livello_Acqua_Massimo;
Segnalazione_Livello_Acqua_Massimo segnalazioni_livello_massimo;
#endif

uint8_t n_pompate=POMPATE;
stato_macchina_t Stato=Stato_ATTESA;

uint8_t IsAcquaVuota = 0, IsAcquaPiena = 0, IsCoperchioAperto = 0, IsPompaOn = 0;
#ifdef MODIFICA_FLIPFLOP_ACQUA
#define ACQUA_PIENA_RILEVATA 0x0A
#define ACQUA_VUOTA_RILEVATA 0xA0
uint16_t Acqua_FlipFlop = ACQUA_VUOTA_RILEVATA; // Consideriamo all'accensione che � da riempire (condizione di default, la prima volta che si avvia va riempita)
uint8_t NeedToWriteFlash = 0;
#endif

uint8_t IsToBuzz = 1;

#ifdef MODIFICA_CONTACICLI_ACQUA
uint8_t NeedToWriteCicli = 0;
#define N_CICLI_DA_MAX	20
uint16_t cicli_trascorsi = 0;
uint8_t fondo_toccato = 0;
#endif

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM16_Init(void);
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void Set_Stato_Pompa(onoff_t OnOff);
void Set_Stato_Phon(onoff_t OnOff);
void Set_Stato_Motore(onoff_t OnOff);
void Spegni_Tutto(void);

void Set_Leds_Ok(onoff_t OnOff);
void Set_Leds_Errore(onoff_t OnOff);

void Buzz(uint16_t time);

void CheckCoperchioAperto();
void CheckAcquaFinita();

void Read_Dipswitch();

#ifdef MODIFICA_FLIPFLOP_ACQUA
void Read_Flash(uint16_t * dataBuf);
void Write_Flash(uint16_t data);
#endif
#ifdef MODIFICA_CONTACICLI_ACQUA
void Read_Flash_CICLI(uint16_t * dataBuf);
void Write_Flash_CICLI(uint16_t data);
#endif
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void HAL_SYSTICK_Callback(void)
{
	if(ms_alive >= 1000)
	{
		ms_alive = 0;
		HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_RESET);
	}
	else
	{
		ms_alive++;
		if(ms_alive == 50) HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_SET);
	}

#if !defined(TEST_DARIO)
	CheckAcquaFinita();
	CheckCoperchioAperto();
	if(ms_ciclo >= 10)
	{
		ms_ciclo = 0;

		if(conta_ciclo) conta_ciclo--;

	}
	else ms_ciclo++;
#endif

	if(ms_buzz) ms_buzz--;
	else
	{
		HAL_TIM_PWM_Stop(&htim16, TIM_CHANNEL_1);
	}
	#ifdef MODIFICA_ASPETTA_ACQUA_LIVELLO_MASSIMO
		if (segnalazioni_livello_massimo.enable == 1)
		{
			if (segnalazioni_livello_massimo.delay > 0)
				segnalazioni_livello_massimo.delay --;
		}
	#endif

}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM16_Init();
  /* USER CODE BEGIN 2 */

  HAL_TIM_PWM_Stop(&htim16, TIM_CHANNEL_1);
  Stato = Stato_ATTESA;
  Set_Leds_Ok(On);
  Set_Leds_Errore(Off);
#ifdef MODIFICA_FLIPFLOP_ACQUA
  Read_Flash(&Acqua_FlipFlop);
#endif

#ifdef MODIFICA_CONTACICLI_ACQUA
  Read_Flash_CICLI(&cicli_trascorsi);
#endif

// Test Dario in Annameryca.h
#if defined (TEST_DARIO)
  while(1)
  {
	  dummy = Tempi_Sel;
	  Read_Dipswitch();

	  if(dummy != Tempi_Sel) Buzz(300);

	  if(INT_H2O_LEVEL_HI_GPIO_Port->IDR & INT_H2O_LEVEL_HI_Pin) Buzz(100);
	  if(INT_H2O_LEVEL_LO_GPIO_Port->IDR & INT_H2O_LEVEL_LO_Pin) Buzz(100);
	  if(BUTTON_START_GPIO_Port->IDR & BUTTON_START_Pin) Buzz(100);
	  if(SAFE_COVER_INT_GPIO_Port->IDR & SAFE_COVER_INT_Pin) Buzz(100);


	  switch(Tempi_Sel)
	  {
	  case 0:
		  Set_Stato_Motore(Off);
		  Set_Stato_Phon(Off);
		  Set_Stato_Pompa(Off);
		  Set_Leds_Ok(On);
		  Set_Leds_Errore(Off);
		  break;
	  case 1:
		  Set_Stato_Motore(On);
		  Set_Stato_Phon(Off);
		  Set_Stato_Pompa(Off);
		  Set_Leds_Ok(Off);
		  Set_Leds_Errore(On);
		  break;
	  case 2:
		  Set_Stato_Motore(Off);
		  Set_Stato_Phon(On);
		  Set_Stato_Pompa(Off);
		  Set_Leds_Ok(Off);
		  Set_Leds_Errore(On);
		  break;
	  case 3:
		  Set_Stato_Motore(Off);
		  Set_Stato_Phon(Off);
		  Set_Stato_Pompa(On);
		  Set_Leds_Ok(Off);
		  Set_Leds_Errore(On);
		  break;
	  default:
		  break;
	  }
  }
#endif
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
#ifdef MODIFICA_FLIPFLOP_ACQUA
	  if(NeedToWriteFlash == 1)
	  {
		  NeedToWriteFlash = 0;
		  Write_Flash(Acqua_FlipFlop);
	  }
#endif

#ifdef MODIFICA_CONTACICLI_ACQUA
	  if(NeedToWriteCicli == 1)
	  {
		  NeedToWriteCicli = 0;
		  Write_Flash_CICLI(cicli_trascorsi);
	  }
#endif

#ifdef MODIFICA_ASPETTA_ACQUA_LIVELLO_MASSIMO
	  if (segnalazioni_livello_massimo.enable == 1)
	  {
		  if (segnalazioni_livello_massimo.delay == 0)
		  {
			  Buzz(1000);
			  segnalazioni_livello_massimo.numero_segnalazioni ++;
			  segnalazioni_livello_massimo.delay = 2000;
			  if (segnalazioni_livello_massimo.numero_segnalazioni >= 2)
			  {
				  segnalazioni_livello_massimo.enable = 0;
				  segnalazioni_livello_massimo.delay = 0;
				  segnalazioni_livello_massimo.numero_segnalazioni = 0;
			  }
		  }
	  }
#endif

	  // Controllo fault
	  if((IsAcquaVuota) || (IsCoperchioAperto))	// Se l'acqua � vuota O il coperchio � aperto
	  {
		  Stato = Stato_ALLARME;
	  }

	  switch (Stato)
	  {
	  case Stato_ATTESA:
		  if(HAL_GPIO_ReadPin(BUTTON_START_GPIO_Port, BUTTON_START_Pin) == GPIO_PIN_SET)	// Se premo il pulsante
		  {
			  if((IsCoperchioAperto == 0) && (IsAcquaVuota == 0))	// e se posso partire, parto (controllo ridondante)
			  {
//				  Buzz(800);
				  Stato=Stato_MOTORE;
				  conta_ciclo = 2300; //conta 2500 centesimi di secondo con il motore acceso
			  }
		  }

		  // Lettura dip (aggiorna Tempi_Sel)
		  Read_Dipswitch();
		  Spegni_Tutto();

		  break;
	  case Stato_MOTORE:
		  Set_Stato_Motore(On);
		  if (conta_ciclo==0)
		  {
//			  Stato=Stato_POMPA;
			  Stato = Stato_POMPA_II;
			  i_pompa = 0;
		  }
		  break;

	  case Stato_POMPA_II:
		  if(conta_ciclo == 0)
		  {
			  if(IsPompaOn) Set_Stato_Pompa(Off);	// Se era acceso, spegni
			  else Set_Stato_Pompa(On);				// Viceversa

			  conta_ciclo = TempiPompa[Tempi_Sel][i_pompa];	// Carica il tempo corretto (sulla base del dipswitch)
			  if(i_pompa >= 10)								// Se sono arrivato alle pompate massime, esco
			  {
				  i_pompa = 0;
				  Stato = Stato_ASCIUGATURA;
				  conta_ciclo = ASCIUGA_TIME;

#ifdef MODIFICA_CONTACICLI_ACQUA
				  if(cicli_trascorsi)
				  {
					  cicli_trascorsi--;
					  NeedToWriteCicli = 1;
				  }
#endif

			  }
			  else i_pompa++;
		  }
		  break;

	  case Stato_POMPA:
		  while (n_pompate > 0)
		  {
			  Set_Stato_Pompa(On);

			  if(n_pompate == POMPATE) conta_ciclo = POMPA_ON + 8;    // Aggiunta
			  else conta_ciclo = POMPA_ON;

			  while (conta_ciclo > 0) if(Stato == Stato_ALLARME) break;
			  Set_Stato_Pompa(Off);
			  conta_ciclo = POMPA_OFF;
			  while (conta_ciclo > 0) if(Stato == Stato_ALLARME) break;
			  n_pompate--;

			  if(Stato == Stato_ALLARME) n_pompate = 0;
		  }


		  n_pompate=POMPATE;
		  Set_Stato_Pompa(Off); // P1OUT &= ~P_POMPA;

		  if(Stato == Stato_ALLARME) break;
		  Stato = Stato_ASCIUGATURA;
		  conta_ciclo = ASCIUGA_TIME; // prima 3500
		  break;

	  case Stato_ASCIUGATURA:
		  Set_Stato_Pompa(Off);
		  Set_Stato_Phon(On);
		  if (conta_ciclo==0) Stato=Stato_STOP;
		  break;
	  case Stato_STOP:
//		  P1OUT &= ~FERMA_TUTTO;	// VEDERE cosa � ferma tutto
		  Spegni_Tutto();

		  conta_ciclo=50;
		  while (conta_ciclo > 0) Set_Leds_Ok(Off);
		  conta_ciclo=50;
		  while (conta_ciclo > 0) Set_Leds_Ok(On);
		  conta_ciclo=50;
		  while (conta_ciclo > 0) Set_Leds_Ok(Off);
		  conta_ciclo=50;
		  while (conta_ciclo > 0) Set_Leds_Ok(On);
		  conta_ciclo=50;
		  while (conta_ciclo > 0) Set_Leds_Ok(Off);
		  conta_ciclo=50;
		  while (conta_ciclo > 0) Set_Leds_Ok(On);
		  conta_ciclo=50;
		  Stato = Stato_ATTESA;
		  break;

	  case Stato_ALLARME:
		  Set_Leds_Ok(Off);
		  Set_Leds_Errore(On);

          #ifdef MODIFICA_ASPETTA_ACQUA_LIVELLO_MASSIMO
		  uint8_t Acqua_Finita = 0;
		  if (IsAcquaVuota)
		  {
			  Acqua_Finita = 1;
		  }
          #endif

		  while((IsCoperchioAperto) || (IsAcquaVuota)) Spegni_Tutto();

		  #ifdef MODIFICA_ASPETTA_ACQUA_LIVELLO_MASSIMO
		  if (Acqua_Finita == 1)
		  {
			  while(IsAcquaPiena != 0)
			  {
				  __NOP();
			  }
			  Acqua_Finita = 0;
			  segnalazioni_livello_massimo.enable = 1;
			  segnalazioni_livello_massimo.delay = 2000;
			  segnalazioni_livello_massimo.numero_segnalazioni = 0;
			  Buzz(1000);
		  }
          #endif

		  Set_Leds_Ok(On);
		  Set_Leds_Errore(Off);

		  Stato = Stato_ATTESA; // se giunge qui sono state ripristinate le condizioni di funzionamento
		  break;
	  case Stato_ERROR:
		  break;
	  default:
		  break;
	  }

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  HAL_RCC_MCOConfig(RCC_MCO, RCC_MCO1SOURCE_SYSCLK, RCC_MCODIV_1);
}

/**
  * @brief TIM16 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM16_Init(void)
{

  /* USER CODE BEGIN TIM16_Init 0 */

  /* USER CODE END TIM16_Init 0 */

  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM16_Init 1 */

  /* USER CODE END TIM16_Init 1 */
  htim16.Instance = TIM16;
  htim16.Init.Prescaler = 0;
  htim16.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim16.Init.Period = 2963;
  htim16.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim16.Init.RepetitionCounter = 0;
  htim16.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim16) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim16) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 1481;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim16, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim16, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM16_Init 2 */

  /* USER CODE END TIM16_Init 2 */
  HAL_TIM_MspPostInit(&htim16);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LED_R_Pin|LED_G_Pin|EN_MOTOR_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LED_OK_COVER_Pin|LED_ALARM_COVER_Pin|EN_WATER_PUMP_Pin|EN_PHON_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : DIP_1_Pin DIP_2_Pin BUTTON_START_Pin */
  GPIO_InitStruct.Pin = DIP_1_Pin|DIP_2_Pin|BUTTON_START_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_R_Pin LED_G_Pin EN_MOTOR_Pin */
  GPIO_InitStruct.Pin = LED_R_Pin|LED_G_Pin|EN_MOTOR_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : INT_H2O_LEVEL_HI_Pin INT_H2O_LEVEL_LO_Pin SAFE_COVER_INT_Pin */
  GPIO_InitStruct.Pin = INT_H2O_LEVEL_HI_Pin|INT_H2O_LEVEL_LO_Pin|SAFE_COVER_INT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_OK_COVER_Pin LED_ALARM_COVER_Pin EN_WATER_PUMP_Pin EN_PHON_Pin */
  GPIO_InitStruct.Pin = LED_OK_COVER_Pin|LED_ALARM_COVER_Pin|EN_WATER_PUMP_Pin|EN_PHON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PA8 */
  GPIO_InitStruct.Pin = GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF0_MCO;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void Set_Stato_Pompa(onoff_t OnOff)
{
	if(OnOff != Off)
	{
		IsPompaOn = 1;
		HAL_GPIO_WritePin(EN_WATER_PUMP_GPIO_Port, EN_WATER_PUMP_Pin, GPIO_PIN_SET);
	}
	else
	{
		IsPompaOn = 0;
		HAL_GPIO_WritePin(EN_WATER_PUMP_GPIO_Port, EN_WATER_PUMP_Pin, GPIO_PIN_RESET);
	}
}

void Set_Stato_Phon(onoff_t OnOff)
{
	if(OnOff != Off)
	{
		HAL_GPIO_WritePin(EN_PHON_GPIO_Port, EN_PHON_Pin, GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(EN_PHON_GPIO_Port, EN_PHON_Pin, GPIO_PIN_RESET);
	}
}

void Set_Stato_Motore(onoff_t OnOff)
{
	if(OnOff != Off)
	{
		HAL_GPIO_WritePin(EN_MOTOR_GPIO_Port, EN_MOTOR_Pin, GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(EN_MOTOR_GPIO_Port, EN_MOTOR_Pin, GPIO_PIN_RESET);
	}
}


void Set_Leds_Ok(onoff_t OnOff)
{
	if(OnOff != Off)
	{
//		HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(LED_OK_COVER_GPIO_Port, LED_OK_COVER_Pin, GPIO_PIN_RESET);
	}
	else
	{
//		HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(LED_OK_COVER_GPIO_Port, LED_OK_COVER_Pin, GPIO_PIN_SET);
	}
}

void Set_Leds_Errore(onoff_t OnOff)
{
	{
		if(OnOff != Off)
		{
			HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(LED_ALARM_COVER_GPIO_Port, LED_ALARM_COVER_Pin, GPIO_PIN_RESET);
		}
		else
		{
			HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(LED_ALARM_COVER_GPIO_Port, LED_ALARM_COVER_Pin, GPIO_PIN_SET);
		}
	}
}

void Buzz(uint16_t time)
{
	ms_buzz = time;
	HAL_TIM_PWM_Start(&htim16, TIM_CHANNEL_1);
}

void Spegni_Tutto()
{
	Set_Stato_Motore(Off);
	Set_Stato_Phon(Off);
	Set_Stato_Pompa(Off);
}

void CheckCoperchioAperto()
{
	if(HAL_GPIO_ReadPin(SAFE_COVER_INT_GPIO_Port, SAFE_COVER_INT_Pin) == GPIO_PIN_SET)
	{
		IsCoperchioAperto = 0;	// Chiuso
	}
	else
	{
		IsCoperchioAperto = 1;	// Aperto
	}
}

void CheckAcquaFinita()
{
#ifdef MODIFICA_CONTACICLI_ACQUA

	if(HAL_GPIO_ReadPin(INT_H2O_LEVEL_HI_GPIO_Port, INT_H2O_LEVEL_HI_Pin) == GPIO_PIN_RESET)
	{
		IsAcquaVuota = 0;	// Ce n'�
		IsAcquaPiena = 1;

		if((cicli_trascorsi == 0) || (fondo_toccato == 1))	// se ho toccato il fondo, o se sono passati abbastanza cicli, buzzo
		{
			fondo_toccato = 0;
			Buzz(3000);
		}

		if(cicli_trascorsi != N_CICLI_DA_MAX)		// Se i cicli quindi sono diversi dal numero massimo, li resetto
		{											// Tale condizione � vera sia che siano 0 sia che siano di pi� ma non max
			cicli_trascorsi = N_CICLI_DA_MAX;
			NeedToWriteCicli = 1;
		}
	}
	else
	{
		IsAcquaPiena = 0;
	}

	if(HAL_GPIO_ReadPin(INT_H2O_LEVEL_LO_GPIO_Port, INT_H2O_LEVEL_LO_Pin) == GPIO_PIN_RESET)
	{
		IsAcquaVuota = 1;	// Finita
		IsAcquaPiena = 0;
		fondo_toccato = 1;
	}
	else
	{
		IsAcquaVuota = 0;
	}

#endif
#ifdef MODIFICA_FLIPFLOP_ACQUA
	if(Acqua_FlipFlop == ACQUA_PIENA_RILEVATA)
	{
		if(HAL_GPIO_ReadPin(INT_H2O_LEVEL_LO_GPIO_Port, INT_H2O_LEVEL_LO_Pin) == GPIO_PIN_RESET)
		{
			Acqua_FlipFlop = ACQUA_VUOTA_RILEVATA;
			NeedToWriteFlash = 1;
			IsAcquaVuota = 1;	// Finita
			IsAcquaPiena = 0;
		}
	}
	else if(Acqua_FlipFlop == ACQUA_VUOTA_RILEVATA)
	{
		if(HAL_GPIO_ReadPin(INT_H2O_LEVEL_HI_GPIO_Port, INT_H2O_LEVEL_HI_Pin) == GPIO_PIN_RESET)
		{
			Acqua_FlipFlop = ACQUA_PIENA_RILEVATA;
			NeedToWriteFlash = 1;
			IsAcquaVuota = 0;	// Ce n'�
			IsAcquaPiena = 1;
			Buzz(3000);
			//			if(IsToBuzz)		// Se devo suonare
			//			{
			//				IsToBuzz = 0;	// Suono (invece di ripristinare l'interrupt lo faccio cos�, col flag)
			//				Buzz(3000);
			//			}
		}
		else if(HAL_GPIO_ReadPin(INT_H2O_LEVEL_LO_GPIO_Port, INT_H2O_LEVEL_LO_Pin) == GPIO_PIN_RESET)
		{
			// Qui siamo n� pieno n� vuoto, non ci preoccupiamo
			//			IsToBuzz = 1;
			IsAcquaVuota = 1;
			IsAcquaPiena = 0;
		}
		else
		{
			IsAcquaVuota = 0;
			IsAcquaPiena = 0;
		}

	}
#endif

}

void Read_Dipswitch(void)
{
	if(HAL_GPIO_ReadPin(DIP_1_GPIO_Port, DIP_1_Pin) == GPIO_PIN_SET)
	{
		if(HAL_GPIO_ReadPin(DIP_2_GPIO_Port, DIP_2_Pin) == GPIO_PIN_SET)
		{
			Tempi_Sel = 3;
		}
		else
		{
			Tempi_Sel = 2;
		}
	}
	else
	{
		if(HAL_GPIO_ReadPin(DIP_2_GPIO_Port, DIP_2_Pin) == GPIO_PIN_SET)
		{
			Tempi_Sel = 1;
		}
		else
		{
			Tempi_Sel = 0;
		}
	}
}


#ifdef MODIFICA_FLIPFLOP_ACQUA
void Read_Flash(uint16_t * dataBuf)
{
//#define FLASH_PAGE0_START 	0x08007800
//#define FLASH_PAGE0_END		0x08007BFF
#define FLASH_PAGE1_START 	0x08007C00
#define FLASH_PAGE1_END		0x08007FFF
	volatile uint16_t * FlashPtr;
	volatile uint16_t TempFlashData = 0;


	FlashPtr = (uint16_t*)(FLASH_PAGE1_END - 1);// Carico il puntatore con l'indirizzo della fine della pagina di salvataggio
	TempFlashData = *FlashPtr;					// Effettuo la prima lettura

	// Mentre trovo dati non validi (a partire dalla cima) E mentre il mio indirizzo di lettura � nel range buono
	while((TempFlashData == 0xFFFF) && (FlashPtr >= (uint16_t*)FLASH_PAGE1_START))
	{
		FlashPtr--;					// Decremento il puntatore
		TempFlashData = *FlashPtr;	// Rileggo
	}

	// Se sono qui, O ho finito lo spazio, O ho trovato un dato valido per cui:

	if(FlashPtr < (uint16_t*)FLASH_PAGE1_START)				// Se ho finito lo spazio (PRIMA VOLTA NELLA STORIA CHE LEGGO)
	{
		FlashPtr = (uint16_t*)FLASH_PAGE1_START;			// Preparo il puntatore
		HAL_FLASH_Unlock();
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, (uint32_t)FlashPtr, (uint64_t)*dataBuf);			// Scrivo il dato
		HAL_FLASH_Lock();									// Blocco
	}
	else if(TempFlashData != 0xFFFF)		// Altrimenti, se ho trovato qualcosa di valido
	{
		*dataBuf = *FlashPtr;				// Prelevo il dato
	}

}

void Write_Flash(uint16_t data)
{
//#define FLASH_PAGE0_START 	0x08007800
//#define FLASH_PAGE0_END		0x08007BFF
#define FLASH_PAGE1_START 	0x08007C00
#define FLASH_PAGE1_END		0x08007FFF

	volatile uint16_t * FlashPtr;
	volatile uint16_t TempFlashData = 0;
	FLASH_EraseInitTypeDef ErasePageInit= {
			.NbPages = 1,
			.PageAddress = FLASH_PAGE1_START,
			.TypeErase = FLASH_TYPEERASE_PAGES
	};
	uint32_t MyPageError = 0;


	FlashPtr = (uint16_t*)(FLASH_PAGE1_START);	// Carico il puntatore con l'indirizzo dell'inizio della pagina di salvataggio
	TempFlashData = *FlashPtr;					// Effettuo la prima lettura

	// Mentre trovo dati validi (a partire dalla base) E mentre il mio indirizzo di lettura � nel range buono (fine memoria 0x8008000, ultimo indirizzo leggibile 0x8007FFE)
	while((TempFlashData != 0xFFFF) && (FlashPtr < (uint16_t*)(FLASH_PAGE1_END - 1)))
	{
		FlashPtr++;					// Incremento il puntatore
		TempFlashData = *FlashPtr;	// Rileggo
	}

	// Se sono qui, O ho finito lo spazio (ho gi� scritto il mio kByte di memoria), O ho trovato posto per scrivere per cui:
	HAL_FLASH_Unlock();										// Sblocco in ogni caso (devo comunque scrivere)

	if(FlashPtr >= (uint16_t*)(FLASH_PAGE1_END - 1))		// Se ho finito lo spazio (ho gi� scritto tutto)
	{
		FlashPtr = (uint16_t*)FLASH_PAGE1_START;			// Preparo il puntatore all'inizio per la scrittura
		HAL_FLASHEx_Erase(&ErasePageInit, &MyPageError);	// Cancello TUTTO

	}
	// Altrimenti conservo l'indirizzo dove ho trovato per la prima volta FFFFh (terreno buono per scrivere) e ci scrivo

	HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, (uint32_t)FlashPtr, (uint64_t)data);			// Scrivo il dato
	HAL_FLASH_Lock();																			// Blocco

}
#endif

#ifdef MODIFICA_CONTACICLI_ACQUA
void Read_Flash_CICLI(uint16_t * dataBuf)
{
#define FLASH_PAGE0_START 	0x08007800
#define FLASH_PAGE0_END		0x08007BFF
//#define FLASH_PAGE1_START 	0x08007C00
//#define FLASH_PAGE1_END		0x08007FFF
	volatile uint16_t * FlashPtr;
	volatile uint16_t TempFlashData = 0;


	FlashPtr = (uint16_t*)(FLASH_PAGE0_END - 1);// Carico il puntatore con l'indirizzo della fine della pagina di salvataggio
	TempFlashData = *FlashPtr;					// Effettuo la prima lettura

	// Mentre trovo dati non validi (a partire dalla cima) E mentre il mio indirizzo di lettura � nel range buono
	while((TempFlashData == 0xFFFF) && (FlashPtr >= (uint16_t*)FLASH_PAGE0_START))
	{
		FlashPtr--;					// Decremento il puntatore
		TempFlashData = *FlashPtr;	// Rileggo
	}

	// Se sono qui, O ho finito lo spazio, O ho trovato un dato valido per cui:

	if(FlashPtr < (uint16_t*)FLASH_PAGE0_START)				// Se ho finito lo spazio (PRIMA VOLTA NELLA STORIA CHE LEGGO)
	{
		FlashPtr = (uint16_t*)FLASH_PAGE0_START;			// Preparo il puntatore
		HAL_FLASH_Unlock();
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, (uint32_t)FlashPtr, (uint64_t)*dataBuf);			// Scrivo il dato
		HAL_FLASH_Lock();									// Blocco
	}
	else if(TempFlashData != 0xFFFF)		// Altrimenti, se ho trovato qualcosa di valido
	{
		*dataBuf = *FlashPtr;				// Prelevo il dato
	}

}

void Write_Flash_CICLI(uint16_t data)
{
#define FLASH_PAGE0_START 	0x08007800
#define FLASH_PAGE0_END		0x08007BFF
//#define FLASH_PAGE1_START 	0x08007C00
//#define FLASH_PAGE1_END		0x08007FFF

	volatile uint16_t * FlashPtr;
	volatile uint16_t TempFlashData = 0;
	FLASH_EraseInitTypeDef ErasePageInit= {
			.NbPages = 1,
			.PageAddress = FLASH_PAGE0_START,
			.TypeErase = FLASH_TYPEERASE_PAGES
	};
	uint32_t MyPageError = 0;


	FlashPtr = (uint16_t*)(FLASH_PAGE0_START);	// Carico il puntatore con l'indirizzo dell'inizio della pagina di salvataggio
	TempFlashData = *FlashPtr;					// Effettuo la prima lettura

	// Mentre trovo dati validi (a partire dalla base) E mentre il mio indirizzo di lettura � nel range buono (fine memoria 0x8008000, ultimo indirizzo leggibile 0x8007FFE)
	while((TempFlashData != 0xFFFF) && (FlashPtr < (uint16_t*)(FLASH_PAGE0_END - 1)))
	{
		FlashPtr++;					// Incremento il puntatore
		TempFlashData = *FlashPtr;	// Rileggo
	}

	// Se sono qui, O ho finito lo spazio (ho gi� scritto il mio kByte di memoria), O ho trovato posto per scrivere per cui:
	HAL_FLASH_Unlock();										// Sblocco in ogni caso (devo comunque scrivere)

	if(FlashPtr >= (uint16_t*)(FLASH_PAGE0_END - 1))		// Se ho finito lo spazio (ho gi� scritto tutto)
	{
		FlashPtr = (uint16_t*)FLASH_PAGE0_START;			// Preparo il puntatore all'inizio per la scrittura
		HAL_FLASHEx_Erase(&ErasePageInit, &MyPageError);	// Cancello TUTTO

	}
	// Altrimenti conservo l'indirizzo dove ho trovato per la prima volta FFFFh (terreno buono per scrivere) e ci scrivo

	HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, (uint32_t)FlashPtr, (uint64_t)data);			// Scrivo il dato
	HAL_FLASH_Lock();																			// Blocco

}

#endif
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
